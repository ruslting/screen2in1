use std::{fs, process::Command, thread, time};

static DEVICES: &'static [&'static str] = &[
    "Wacom HID 531A Pen stylus",
    "Wacom HID 531A Pen eraser",
    "Wacom HID 531A Finger touch",
];

const WAITING_TIME: time::Duration = time::Duration::from_millis(2000);
// const N_STATES: u32 = 4;
const ACCEL_G: i32 = 20000;

#[derive(PartialEq, Clone, Copy)]
enum Rotation {
    Normal,
    Inverted,
    Left,
    Right,
}

// TODO better way to choose orientation ?
impl Rotation {
    fn update(&mut self, accel: [i32; 2]) -> bool {
        let old = *self;
        if accel[0].abs() <= accel[1].abs() {
            if accel[1] > ACCEL_G {
                *self = Rotation::Inverted;
            } else if accel[1] < -ACCEL_G {
                *self = Rotation::Normal;
            }
        } else {
            if accel[0] > ACCEL_G {
                *self = Rotation::Left;
            } else if accel[0] < -ACCEL_G {
                *self = Rotation::Right;
            }
        }

        old != *self
    }

    fn name(&self) -> &str {
        match *self {
            Rotation::Normal => "normal",
            Rotation::Inverted => "inverted",
            Rotation::Left => "left",
            Rotation::Right => "right",
        }
    }

    fn coord(&self) -> [&str; 9] {
        match *self {
            Rotation::Normal => ["1", "0", "0", "0", "1", "0", "0", "0", "1"],
            Rotation::Inverted => ["-1", "0", "1", "0", "-1", "1", "0", "0", "1"],
            Rotation::Left => ["0", "-1", "1", "1", "0", "0", "0", "0", "1"],
            Rotation::Right => ["0", "1", "0", "-1", "0", "1", "0", "0", "1"],
        }
    }
}

fn get_rotation(path_x: &str, path_y: &str) -> [i32; 2] {
    let accel_x: i32 = fs::read_to_string(path_x)
        .expect("Failed to read rotation file")
        .trim()
        .parse()
        .expect("failed to parse x acceleration into int");
    let accel_y: i32 = fs::read_to_string(path_y)
        .expect("Failed to read rotation file")
        .trim()
        .parse()
        .expect("failed to parse y acceleration into int");

    return [accel_x, accel_y];
}

fn rotate_screen(rotation: Rotation) {
    let coord = rotation.coord();
    let name = rotation.name();
    Command::new("xrandr")
        .arg("-o")
        .arg(name)
        .spawn()
        .expect("failed to rotate screen");

    for device in DEVICES {
        Command::new("xinput")
            .arg("set-prop")
            .arg(device)
            .arg("Coordinate Transformation Matrix")
            .args(coord)
            .output()
            .expect("failed to execute process");
    }
}

fn main() {
    let output = Command::new("bash")
        .arg("-c")
        .arg("ls /sys/bus/iio/devices/iio:device*/in_accel_*_raw")
        .output()
        .expect("failed to execute process")
        .stdout;
    let mut paths = std::str::from_utf8(&output)
        .expect("Output of ls should be a string")
        .lines();

    let accel_x_path = paths.next().unwrap();
    let accel_y_path = paths.next().unwrap();

    let mut rotation = Rotation::Normal;

    loop {
        let accel = get_rotation(accel_x_path, accel_y_path);

        if rotation.update(accel) {
            rotate_screen(rotation);
        }

        thread::sleep(WAITING_TIME);
    }
}
